clc
clear all
close all

%% Constants
% constants
f_rated = 50; % % Hz
% Number of Pole Pairs
NPP = 2;
% No. of WT
NoWT = 15;
s_rated = -0.8e-2; % % slip

omega_rated = (2*pi*f_rated)/NPP;

omega_wtr_rated_RPM = 15; % %  rpm
omega_wtr_rad = omega_wtr_rated_RPM*(pi/30);

% %  rated rotor speed:
omega_rotor_rated = (1-s_rated)*omega_rated;

n_gear = (omega_rated*(1-s_rated))/omega_wtr_rad;

% %  Generator model
Rr = 1.5e-3; % % Ohm
Lr = 0.05e-3; % % H
Rs = 1.1e-3; % % ohm
Ls = 0.1e-3; % % H
Lm = 2.13e-3; % % H

% % Transformer model
R1 = 2e-3; R2 = R1;
L1 = 2e-6; L2 = L1;

% % Cable model
R_cable = 0.1; %Ohm/km
L_cable = 0.5e-3; % %mH / km
cable_length = 20; % Km length of cable
C_cable = 0.1e-6; % µF/Km

% Rated values
VT_rated =  690; % % V
VT_ph = 690/sqrt(3);
omega_r = omega_rotor_rated;
P_mech = -2.3251531211e6;
v_f = 2.5361129189402436;
%transformer turns ratio
a = VT_rated/33e3;


% % s = 1-(omega_r/(x(4)/x(5))
%% MPPT
k = P_mech/(omega_wtr_rad^3);
%create vector from 0 to 15 RPM
omega_vec_RPM = 1:2:15;
%omega_vec = linspace(0.1, omega_wtr_rad, 25);
omega_vec = omega_vec_RPM * pi/30;

%compute MPPT
MPPT = k*omega_vec.^3;


%% Solve generator equations as function of MPPT and WTR speed
% (rated values)%
rated_val = [1914.685040+1385.284266i, 2009.130767+253.362819i, 94.445726-1131.921447i, 398.371+0.01i, 157.080+0.001i, -0.008 + 0.0001i];

options = optimoptions('fsolve','Algorithm', 'trust-region-dogleg', 'Display', 'iter', 'UseParallel', true, 'FunValCheck', 'off',  'FiniteDifferenceType', 'central', 'MaxIter',500e3,'MaxFunEvals', 500e3);

x = zeros(length(omega_vec), 6);
for qq = 1:length(omega_vec)
    
    %Initial values using known rated values and then linear scaling
    x0 = ((rated_val+0.01+0.01i)/length(omega_vec)) .*qq;
    x0 = euler_form(x0);
    
    % solve equations
    [x(qq, :), fval] = fsolve(@(x)myfunc(x, MPPT(qq), omega_vec(qq)*n_gear), x0, options);
end

%% assign solutions to their real names
I1 = x(:, 1);
I2 = x(:, 2);
I3 = x(:, 3);
VT = real(x(:, 4));
omega_s = real(x(:, 5));
s = real(x(:,6));

%% Read results from table to avoid re computing
%save('x.mat','x');


%% TASK 1
%%
%plot V and f
figure(1)
plot(real(omega_s)/(2*pi), VT*sqrt(3), 'r-o', 'Linewidth', 1.5)
hold on
plot(real(omega_s)/(2*pi), omega_s.*v_f.*sqrt(3), 'black', 'Linewidth', 1.5)
xlabel('Stator frequency[Hz]')
ylabel('Terminal Voltage[V]')
legend('Computed', 'Fit', 'Location', 'northwest')
grid()
exportgraphics(gca, 'VT_freq.pdf')

sprintf('rated Terminal voltage = %0.4f', real(x(end, 4)))
sprintf('Rated stator speed = %0.4f rad/s', real(x(end, 5)))
iter = 1:6;
compose("x%d : abs = %3.3f, real = %3.3f, imag = %3.2f", [iter', abs(x(end, :))', real(x(end, :))', imag(x(end, :))'])


%% Compute Losses

% % Rotor side
Ploss_rotor = 3*Rr*(abs(I2)).^2;
% % Stator side
Ploss_stator = 3*Rs .*(abs(I1)).^2;

Q_rotor = 3j*(Lr.*omega_vec'*n_gear) .* (abs(I2)).^2;

Q_shunt = 3j*(Lm.*omega_s) .*(abs(I3)).^2;

Q_stator = 3j*(Ls.*omega_s) .*(abs(I1)).^2;

Ploss = Ploss_rotor+Ploss_stator;
Q_loss = Q_rotor + Q_shunt+ Q_stator;

% Complex loss in generator
S_SCIG = Ploss + MPPT' + Q_loss;
%% Compute Reactive Power
%S_pwr = 3*VT.*conj(I1);

%% Plot power
plot_vec = {abs(MPPT'), Ploss, abs(S_SCIG)};
y_text = ["Mechanical Power[MW]", "SQIG Active power loss[kW]", " SQCG complex power[MVA]"];
% Scale to fitting SI units
scale = [1e-6, 1e-3, 1e-6];
filename = ["Mech_Power.pdf", "SCIG_loss.pdf", "S_SCIG.pdf"];

% % plot loop
for p =  1:3
    figure(p+1)
    plot(omega_vec*30/pi, plot_vec{p}*scale(p), '-o', 'Linewidth', 1.5)
    xlabel('Wind Turbine rotor speed[RPM]')
    ylabel(y_text(p))
    grid()
    exportgraphics(gca, filename(p))
end

%% Create required table for ever 2 RPM
%RPM_vec = omega_vec*30/pi;
%task1_table = table(plot_vec{1}*scale(1), plot_vec{2}*scale(2), plot_vec{3}*scale(3));    
%task1_table(:, 4) = omega_vec_RPM;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ############ Task 2 ###############
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute real power output from Grid side converter
P_GrVSC = abs(real(S_SCIG)).*0.99^2;

% compute reactive power output from Grid side converter as 10% of active
% power
Q_GrVSC = P_GrVSC.*0.1;

S_GrVSC = P_GrVSC + Q_GrVSC*1j;
%% Per phase analysis of transformer
% Per phase primary curent in transformer
I_trafo_prime = conj(S_GrVSC./(3.*VT_ph));
% Per phase secondary side current:


% Primary side active and reactive power loss
Pp_trafo = 3 .*R1 .* abs(I_trafo_prime).^2;
Qp_trafo = 3 .*L1*(f_rated*2*pi) .*abs(I_trafo_prime).^2;

% Secondary side active and reactive power loss
Ps_trafo = 3 .*(R2) .* abs(I_trafo_prime).^2;
Qs_trafo = 3 .*L2*(f_rated*2*pi) .*abs(I_trafo_prime).^2;

% Total complex power loss in transformer
S_trafo_loss = Pp_trafo + Ps_trafo + 1j*(Qp_trafo + Qs_trafo);

% Total complex power in transformer
S_trafo = S_GrVSC - S_trafo_loss;

%% Per phase analysis of cable
V_trafo = 33e3/sqrt(3);

% Per phase input current in cable
I_cable = NoWT* conj(S_trafo./(3*V_trafo));

% Reactance of series Inductance
XL = 1j*L_cable*f_rated*2*pi;
% Voltage drop along cable:
V_cable_drop = cable_length*(R_cable* + XL).*I_cable;

% Voltage at POC
V_POC = V_trafo - V_cable_drop;

% Reactance of shunt capacitance
Xc = -1j/(2*pi*C_cable*cable_length);

% Active and Reactive Power lossses
P_cable_loss = 3 * cable_length * R_cable .* abs(I_cable).^2;
Q1_cable = 3 * cable_length * XL .* abs(I_cable).^2;
Q2_cable = 3.* (abs(V_POC).^2)./(cable_length * Xc);

% Total complex losses in cable
S_cable_loss = P_cable_loss + Q1_cable + Q2_cable ;

% Total complex power at POC
S_POC = (NoWT * S_trafo) - S_cable_loss ; 

%% PLOT active and reactive power available active and reative power at POC

figure()
% Active power
plot(omega_vec_RPM, real(S_POC)*1e-6, 'r-o', 'Linewidth', 1.5)
xlabel('WT rotor speed[RPM]')
ylabel('Active Power[MW]')
title('Active Power at POC')
grid()
exportgraphics(gca, 'P_POC.pdf')

figure()
plot(omega_vec_RPM, imag(S_POC)*1e-3, 'b-o', 'Linewidth', 1.5)
xlabel('WT rotor speed[RPM]')
ylabel('Reactive Power[kVAr]')
title('Reactive Power at POC')
grid()
exportgraphics(gca, 'Q_POC.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ############ Task 3###############
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Efficiency for all speeds
eta = real(S_POC)./(NoWT .*abs(real(S_SCIG)));

figure()
plot(omega_vec_RPM, eta *1e2, 'b-o', 'Linewidth', 1.5)
xlabel('WT rotor speed[RPM]')
ylabel('Effeciency of power transfer[%]')
grid()
exportgraphics(gca, 'efficiency.pdf')




















         
     

