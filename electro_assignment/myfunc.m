function F = myfunc(x, MPPT, omega_vec)

%% Constants

f_rated = 50; % % Hz

NPP = 2;
s_rated = -0.8e-2; % % slip

omega_rated = (2*pi*f_rated)/NPP;

omega_wtr_rated_RPM = 15; % %  rpm
omega_wtr_rad = omega_wtr_rated_RPM*(pi/30);

% %  rated rotor speed:
omega_rotor_rated = (1-s_rated)*omega_rated;
n_gear = (omega_rated*(1-s_rated))/omega_wtr_rad;

% %  Generator model
Rr = 1.5e-3; % % Ohm
Lr = 0.05e-3; % % H
Rs = 1.1e-3; % % ohm
Ls = 0.1e-3; % % H
Lm = 2.13e-3; % % H
VT_rated =  690; % % V
VT_ph = 690/sqrt(3);
omega_r = omega_rotor_rated;
% Mechanical rated power
P_mech = -2.3251531211e6;
% %V/omega_s
k = 2.5361129189402436;


%% Equations to solve

%% Equations to solve
F(1) = Rs*x(1) + x(4)- x(3)*1i*Lm* (x(5));
%F(2) = x(2)*Rr*((1-(1-(omega_vec./(x(4)/k))))./(1-(omega_vec./(x(4)/k)))) + 1i*x(2).*Lr.*omega_vec + x(2)*Rr + 1i*x(3)*Lm*k;
F(2) =  x(2).* Rr + x(2).*Rr .* ((1- (x(6)))./ (x(6)))  + 1i*x(2).*Lr.*omega_vec + x(2)*Rr + 1i*x(3)*Lm.* (x(5)); 
F(3) = x(2) - x(1) -x(3);
F(4) = x(4) - (x(5))*k;
F(5) = abs(x(2)).*abs(x(2)).*(3.*Rr.*((1 -  (x(6)))./ (x(6)))) - MPPT;
F(6) = ( (x(5)) - omega_vec)./ (x(5)) -  (x(6));
end



%%%% x(:, 2).*Rr.*((1-(1-(omega_vec./(x(:, 4)/k))))./(1-(omega_vec./(x(:, 4)/k)))) + 1i*x(:, 2)*Lr.*omega_vec + x(:, 2)*Rr + 1i*x(:, 3)*Lm*k;