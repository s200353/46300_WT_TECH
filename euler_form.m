function x = euler_form(x0)

x = zeros(length(x0), 1);
for i = 1:length(x0)
    %compute length of complex number
    r = abs(x0(i));
    
    %compute angle
    theta = angle(x0(i));
    
    x(i) = r*exp(theta*j);
end

end

    
